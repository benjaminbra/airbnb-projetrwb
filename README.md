# AirBnB-ProjetRWB

Ce projet a été réalisé dans le cadre du cours de Web Mobile.

## Librairies utilisées

- [Bootstrap 4.1.3](https://getbootstrap.com/)
- [Jquery 3.3.1](https://code.jquery.com/jquery-3.3.1.js)
- [Leaflet](https://leafletjs.com/)
- [FontAwesome](https://fontawesome.com/)

## Avancement
- [X] Gestion du responsive pour les différents écrans

### Desktop
- [X] Affichage de la carte
- [X] Affichage de la liste des locations
- [X] Affichage du bloc de filtres
- [ ] Gestion des filtres

### Page tablette
- [X] Gestion du "toggle" entre la liste et la carte

### Page mobile
- [X] Bloc de filtres déplacé au dessus
- [X] Gestion de la réduction du bloc de filtre
- [ ] Système d'accordéon pour les filtres

## Generation du jeu de données

Si le bloc du milieu laisse une page blanche, il est nécessaire de suivre les étapes ci-dessous.

### 1. Générer le jeu de données

Aller sur [JSON Generator](https://www.json-generator.com/)

Pattern utilisé :
```
[
  '{{repeat(100,300)}}',
  {
    name: '{{random("Belle", "Grande", "Petite")}} {{random("Location", "Maison", "Chambre")}} {{random("Hypercentre", "Vue exceptionnelle", "Sans intérêt")}}',
    type: '{{random("Logement entier", "Chambre privée")}}',
    nbLits: '{{random(1,2,3)}}',
    nbDiapo: '{{random(2,3,4)}}',
    prix: '{{integer(5,200)}}',
    note: '{{integer(1,5)}}'
  }
]
```

### 2. Exporter le jeu de données

Cliquer sur `Upload JSON to server` puis `Copy JSON file URL`.

Remplacer l'URL de `app.js` à la ligne 10.
