$(document).ready(function(){
    $('#nbLogement').text('500');

    var map = L.map('mapid').setView([47.2216603,-1.5663579], 13);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    var liste = $.getJSON('http://www.json-generator.com/api/json/get/cknyAZaofC?indent=2', function(d){
        $('#nbLogement').text(d.length);
        $.each(d, function(key, val){
            addLogement(val);
        });
    });

    $('input[name="toggle"]').on('change', function(){
        $('#listblock').toggleClass('d-none d-xl-block');
        $('#mapblock').toggleClass('d-none d-xl-block');
    });

    $('.responsive-button-menu').on('click', function () {
        $('.filter-body').toggleClass('d-none d-lg-block');
    });
    
});

var imageList = [1054,1062,859,594,234,221];

function addLogement(logement){
    $('#cityList').append('<div class="card col-sm-12 col-md-4">\n' +
        '                    <img class="card-img-top" src="https://picsum.photos/250/165?image=' + imageList[Math.floor(Math.random() * Math.floor(imageList.length - 1))] + '" alt="Card Image"/>\n' +
        '                    <div class="card-body">\n' +
        '                        <h5 id="titre" class="card-title">' + logement.name + '</h5>\n' +
        '                        <div class="line-red">\n' +
        '                            <span>' + logement.type + '</span> · <span>' + logement.nbLits + '</span> Lits\n' +
        '                        </div>\n' +
        '                        <div>\n' + logement.prix + '€ par nuit\n </div>\n' +
        '                    </div>\n' +
        '                </div>');
}